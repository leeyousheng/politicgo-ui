import { render } from '@testing-library/react';
import { createMemoryHistory } from 'history';
import React from 'react';
import { Router } from 'react-router-dom';
import App from './App';

test('renders common components', () => {
  const history = createMemoryHistory()
  const { getByTestId } = render(
    <Router history={history}>
      <App user={null} />
    </Router>
  )
  expect(getByTestId('comp-appbar')).toBeInTheDocument()
});

test('renders feed page when no such path is given', () => {
  const history = createMemoryHistory()
  history.push('/some/other/path')
  const { getByTestId } = render(
    <Router history={history}>
      <App user={null} />
    </Router>
  )
  expect(getByTestId('page-feed')).toBeInTheDocument()
});

test('renders feed page when root path is given', () => {
  const history = createMemoryHistory()
  history.push('/')
  const { getByTestId } = render(
    <Router history={history}>
      <App user={null} />
    </Router>
  )
  expect(getByTestId('page-feed')).toBeInTheDocument()
});
