import { withStyles } from '@material-ui/core';
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Appbar from '../components/appbar';
import Provider from '../components/provider';
import Feed from '../pages/Feed';
import Login from '../pages/Login';
import Profile from '../pages/Profile';
import { app as styles } from '../styles/app';

interface Props {
  user: firebase.User | null,
  isTokenSet: boolean
  classes: {
    app: string,
    content: string,
  },
};

const App = withStyles(styles)(({ classes, user, isTokenSet }: Props) => (
  <div className={classes.app}>
    <Provider user={user} isTokenSet={isTokenSet}>
      <Appbar />
      <div className={classes.content}>
        <Switch>
          <Route path="/login" component={Login} />
          <Route path="/profile" component={Profile} />
          <Route path="/" component={Feed} />
        </Switch>
      </div>
    </Provider>
  </div >
));

export default App;
