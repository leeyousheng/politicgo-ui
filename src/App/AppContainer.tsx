import Axios, { default as axios } from 'axios';
import firebase from 'firebase/app';
import React, { useEffect, useState } from 'react';
import firebaseConfig from '../firebase.secret';
import App from './App';

firebase.initializeApp(firebaseConfig);
axios.defaults.baseURL = process.env.REACT_APP_API_ROOT;

const AppContainer = () => {
  const [currentUser, setCurrentUser] = useState<firebase.User | null>(null);
  const [idToken, setIdToken] = useState<string>("");

  Axios.defaults.headers.common.Authorization = idToken;

  useEffect(() => {
    const removeListener = firebase.auth().onAuthStateChanged((user) => { setCurrentUser(user); });
    return () => removeListener();
  }, []);

  useEffect(() => {
    const setAxiosAuth = async () => setIdToken(!!currentUser ? `Bearer ${await currentUser.getIdToken()}` : "");
    setAxiosAuth();
  }, [currentUser]);

  return <App user={currentUser} isTokenSet={idToken !== ""} />;
}

export default AppContainer;
