import Axios from "axios";
import { Page } from "../../models/page";
import { Post } from "../../models/post";
import { feedUrl } from "../routes";
import { PageView } from "../views/page";
import { convertPostViewToPost, PostView } from "./views";

export const getFeed = async (cursor?: string) => {
  const params = { cursor, size: 1 }
  try {
    const res = await Axios.get(feedUrl, { params });

    const data = res.data as PageView<PostView>;
    return new Page<Post>(
      data.items.map(v => convertPostViewToPost(v)),
      data.hasMore,
    )
  } catch (e) {
    alert(e)
  }
}