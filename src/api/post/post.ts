import Axios from "axios";
import { Page } from "../../models/page";
import { Post } from "../../models/post";
import { postUrl } from "../routes";
import { PageView } from "../views/page";
import { convertPostViewToPost, CreatePostView, PostView } from "./views";

export const getPosts = async () => {
  try {
    const res = await Axios.get(postUrl);

    const data = res.data as PageView<PostView>
    const items = data.items.map(v => convertPostViewToPost(v));
    return new Page<Post>(items, data.hasMore)
  } catch (e) {
    alert(e);
  }
}

export const createPost = async (text: string, topics?: Array<string>) => {
  const post: CreatePostView = {
    text: text,
    topics: !!topics ? topics : []
  }

  try {
    const response = await Axios.post(postUrl, post);
    return convertPostViewToPost(response.data);
  } catch (e) {
    alert(e);
  }
}

export default { getPosts, createPost };