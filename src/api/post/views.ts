import { Post } from "../../models/post";

export interface PostView {
  createdBy: string;
  createdOn: Date;
  id: string;
  text: string;
  author: {
    name: string;
    photoURL: string;
  };
  topics: Array<string>;
};

export interface CreatePostView {
  text: string;
  topics: Array<string>;
}

export const convertPostViewToPost = (v: PostView): Post => ({
  createdBy: v.createdBy,
  createdOn: v.createdOn,
  text: v.text,
  id: v.id,
  photoURL: v.author.photoURL,
  name: v.author.name,
  topics: v.topics,
});