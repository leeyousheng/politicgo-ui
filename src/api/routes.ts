export const postUrl = '/posts';
export const feedUrl = '/feed';

export default { postUrl, feedUrl };