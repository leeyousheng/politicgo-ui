export interface PageView<T> {
  items: Array<T>
  hasMore: boolean
};
