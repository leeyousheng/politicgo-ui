import { render } from '@testing-library/react';
import { createMemoryHistory } from 'history';
import React from 'react';
import { Router } from 'react-router-dom';
import Appbar from './Appbar';

test('appbar items are present when not signed in', () => {
  const history = createMemoryHistory()
  const { getByTestId } = render(<Router history={history}><Appbar user={null} /></Router>);
  expect(getByTestId('comp-appbar')).toBeInTheDocument();
  expect(getByTestId('appbar-branding-link')).toBeInTheDocument();
  expect(getByTestId('appbar-searchbar')).toBeInTheDocument();
  expect(getByTestId('appbar-loginbutton')).toBeInTheDocument();
  expect(getByTestId('appbar-menu')).toBeInTheDocument();
});

test('appbar items are present when signed in', () => {
  const history = createMemoryHistory()
  const { getByTestId } = render(<Router history={history}><Appbar user={null} /></Router>);
  expect(getByTestId('comp-appbar')).toBeInTheDocument();
  expect(getByTestId('appbar-branding-link')).toBeInTheDocument();
  expect(getByTestId('appbar-searchbar')).toBeInTheDocument();
  expect(getByTestId('appbar-menu')).toBeInTheDocument();
});