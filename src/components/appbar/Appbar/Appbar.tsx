import { withStyles } from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import ToolBar from '@material-ui/core/Toolbar';
import React from 'react';
import { appbar as styles } from '../../../styles/appbar';
import Branding from '../Branding';
import LoginSignup from '../LoginSignup';
import Menu from '../Menu';
import SearchBar from '../Searchbar';

interface Props {
  user: firebase.User | null,
  classes: {
    midSpace: string,
  },
};

const Appbar = withStyles(styles)(({ user, classes }: Props) => (
  <AppBar data-testid="comp-appbar">
    <ToolBar >
      <Branding />
      <div className={classes.midSpace}>
        <SearchBar />
      </div>
      {!user ? <LoginSignup /> : null}
      <Menu user={user} />
    </ToolBar>
  </AppBar>
));

export default Appbar;
