import React, { useContext } from 'react';
import { UserContext } from '../../provider';
import Appbar from './Appbar';

const AppbarContainer = () => {
  const user = useContext(UserContext);
  return (<Appbar user={user} />);
}

export default AppbarContainer;
