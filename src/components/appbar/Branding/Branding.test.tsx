import { fireEvent, render } from '@testing-library/react';
import { createMemoryHistory } from 'history';
import React from 'react';
import { Router } from 'react-router-dom';
import Branding from './Branding';

test('click on branding should route to root path', () => {
  const history = createMemoryHistory();
  history.push('/profile');
  expect(history.location.pathname).toEqual('/profile')
  const { getByTestId } = render(
    <Router history={history}>
      <Branding />
    </Router>
  )

  expect(getByTestId('appbar-branding-link')).toBeInTheDocument();
  fireEvent.click(getByTestId('appbar-branding-link'));
  expect(history.location.pathname).toEqual('/');
});