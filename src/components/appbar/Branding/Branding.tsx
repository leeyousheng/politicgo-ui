import { Typography, withStyles } from '@material-ui/core';
import React from 'react';
import { Link } from 'react-router-dom';
import { branding as styles } from '../../../styles/appbar';

export interface Props {
  classes: {
    branding: string;
  };
}

const Branding = withStyles(styles)(({ classes }: Props) => {
  return (
    <div>
      <Link data-testid="appbar-branding-link" to='/' style={{ textDecoration: 'none' }} className={classes.branding} >
        <Typography>Politic Go</Typography>
      </Link>
    </div>
  )
});

export default Branding;
