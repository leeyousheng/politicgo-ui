import { Button } from '@material-ui/core';
import React from 'react';

interface Props {
  onClick: () => void,
};

const LoginSignup = ({ onClick }: Props) => (
  <Button
    data-testid="appbar-loginbutton"
    variant="contained"
    color="primary"
    onClick={onClick}
  >
    Log In / Sign Up
  </Button>
)

export default LoginSignup;
