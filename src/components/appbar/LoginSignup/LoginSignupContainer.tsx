import React from 'react';
import { useHistory } from 'react-router-dom';
import LoginSignup from './LoginSignup';

const LoginSignupContainer = () => {
  const history = useHistory();
  const onClick = () => { history.push('/login'); };

  return (
    <LoginSignup onClick={onClick} />
  );
};

export default LoginSignupContainer;
