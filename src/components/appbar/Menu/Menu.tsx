import { ListItemIcon, ListItemText, Menu as MUIMenu, MenuItem, withStyles } from '@material-ui/core';
import React from 'react';
import { menu as styles } from '../../../styles/appbar';
import { Item } from './types';

interface Props {
  anchorEl: null | HTMLElement;
  handleClose: () => void;
  classes: {
    menu: string;
    itemIcon: string;
  };
  items: Array<Item>;
};

const Menu = withStyles(styles)(({ classes, anchorEl, handleClose, items }: Props) => (
  // TODO: Warning due to deprecation issue: https://github.com/mui-org/material-ui/issues/13394
  <MUIMenu
    className={classes.menu}
    id="customized-menu"
    anchorEl={anchorEl}
    keepMounted
    open={Boolean(anchorEl)}
    onClose={handleClose}
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
  >
    {
      items.map(({ icon, label, key, onClick }: Item) => (
        <MenuItem key={key} onClick={(event: React.MouseEvent<HTMLElement>) => {
          onClick(event);
          handleClose();
        }}>
          <ListItemIcon className={classes.itemIcon}>
            {icon}
          </ListItemIcon>
          <ListItemText primary={label} />
        </MenuItem>
      ))
    }
  </MUIMenu>)
);

export default Menu;
