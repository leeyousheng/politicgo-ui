import { Avatar, Button, withStyles } from '@material-ui/core';
import { ArrowDropDown } from '@material-ui/icons';
import React from 'react';
import { profile as styles } from '../../../styles/appbar';

export interface Props {
  classes: {
    button: string;
    avatar: string;
    dropdown: string;
  };
  handleClick: (event: React.MouseEvent<HTMLElement>) => void;
  user: firebase.User | null
};

const Profile = withStyles(styles)(({ classes, handleClick, user }: Props) => (
  <Button
    className={classes.button}
    aria-controls='menu'
    aria-haspopup='true'
    onClick={handleClick}
  >
    <Avatar className={classes.avatar} src={user && user.photoURL ? user.photoURL : undefined} />
    <ArrowDropDown className={classes.dropdown} />
  </Button>
));

export default Profile;
