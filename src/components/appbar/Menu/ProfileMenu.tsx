import React from 'react';
import Menu from './Menu';
import Profile from './Profile';
import { Item } from './types';

interface Props {
  anchorEl: null | HTMLElement;
  handleProfileClick: (event: React.MouseEvent<HTMLElement>) => void;
  handleMenuClose: () => void;
  user: firebase.User | null;
  items: Array<Item>;
};

const ProfileMenu = ({ handleProfileClick, user, anchorEl, handleMenuClose, items }: Props) => (
  <div data-testid="appbar-menu">
    <Profile handleClick={handleProfileClick} user={user} />
    <Menu anchorEl={anchorEl} handleClose={handleMenuClose} items={items} />
  </div>
);

export default ProfileMenu;
