import { AccountCircle as AccountCircleIcon, ExitToApp as ExitToAppIcon } from '@material-ui/icons';
import firebase from 'firebase/app';
import 'firebase/auth';
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import ProfileMenu from './ProfileMenu';
import { Item } from './types';

interface Props {
  user: firebase.User | null
}

const ProfileMenuContainer = ({ user }: Props) => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const history = useHistory();

  const handleProfileClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const items = (signedIn: boolean): Array<Item> => {
    const itemList: Array<Item> = [];

    if (signedIn) {
      itemList.push({
        key: 'profile',
        icon: <AccountCircleIcon fontSize="small" />,
        label: 'My Profile',
        onClick: () => {
          history.push('/profile');
        }
      })
    }

    if (!signedIn) {
      itemList.push({
        key: 'login',
        icon: <ExitToAppIcon fontSize="small" />,
        label: 'Log In / Sign Up',
        onClick: () => {
          history.push('/login');
        }
      })
    } else {
      itemList.push({
        key: 'logout',
        icon: <ExitToAppIcon fontSize="small" />,
        label: 'Log Out',
        onClick: () => {
          firebase.auth().signOut();
        },
      })
    }

    return itemList
  }

  return (
    <ProfileMenu
      anchorEl={anchorEl}
      handleProfileClick={handleProfileClick}
      handleMenuClose={handleMenuClose}
      user={user}
      items={items(!!user)}
    />
  );
}

export default ProfileMenuContainer;
