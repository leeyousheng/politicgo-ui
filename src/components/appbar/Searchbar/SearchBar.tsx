import { InputBase, withStyles } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import React from 'react';
import { searchBar as styles } from '../../../styles/appbar';

export interface Props {
  classes: {
    search: string;
    searchIcon: string;
    inputRoot: string;
    inputInput: string;
  };
}

const SearchBar = withStyles(styles)(({ classes }: Props) => {
  return (
    <div data-testid="appbar-searchbar" className={classes.search}>
      <div className={classes.searchIcon}>
        <SearchIcon />
      </div>
      <InputBase
        placeholder="Search"
        classes={{
          root: classes.inputRoot,
          input: classes.inputInput,
        }}
        inputProps={{ 'aria-label': 'search' }}
      />
    </div>
  )
});

export default SearchBar;
