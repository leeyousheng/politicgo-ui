import React from 'react';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import { UiConfig } from './types';

interface Props {
  uiConfig: UiConfig,
  auth: firebase.auth.Auth,
};

const Login = ({ uiConfig, auth }: Props) => (
  <StyledFirebaseAuth
    uiConfig={uiConfig}
    firebaseAuth={auth}
  />
)

export default Login;
