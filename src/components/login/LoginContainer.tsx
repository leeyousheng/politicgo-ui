import firebase from 'firebase/app';
import 'firebase/auth';
import React from 'react';
import Login from './Login';

const LoginContainer = () => {
  return (
    <Login
      uiConfig={{
        signInFlow: 'popup',
        signInSuccessUrl: '/',
        signInOptions: [
          firebase.auth.GoogleAuthProvider.PROVIDER_ID,
          firebase.auth.EmailAuthProvider.PROVIDER_ID,
        ],
      }}
      auth={firebase.auth()}
    />
  )
}

export default LoginContainer;
