export interface UiConfig {
  signInFlow: string,
  signInSuccessUrl: string,
  signInOptions: Array<string>,
};
