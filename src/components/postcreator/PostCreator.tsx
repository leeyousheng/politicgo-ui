import { Avatar, Grid, Input, Paper, withStyles } from '@material-ui/core';
import React from 'react';
import { postCreator as styles } from '../../styles/feed';
import CreationModal from './creationModal';

interface Props {
  classes: {
    root: string,
    input: string,
  },
  text: string,
  topics: Array<string>
  updateTopics: (topics: Array<string>) => void
  modalOpen: boolean,
  user: firebase.User | null,
  updateText: (value: string) => void
  openModal: () => void
  closeModal: () => void
  submitPost: (text: string) => void
};

const PostCreator = withStyles(styles)(({ classes, text, topics, updateTopics, modalOpen, user, updateText, openModal, closeModal, submitPost }: Props) => {
  const handleOnInputClick = () => openModal();

  return (
    <div>
      <Paper className={classes.root}>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid item>
            <Avatar aria-label={user && user.displayName ? user.displayName : "anon"} src={user && user.photoURL ? user.photoURL : undefined} />
          </Grid>
          <Grid item xs={true}>
            <Input className={classes.input} placeholder="What is the issue" value={text} onClick={handleOnInputClick} />
          </Grid>
        </Grid>
      </Paper>
      <CreationModal text={text} topics={topics} open={modalOpen} closeModal={closeModal} updateText={updateText} submitPost={submitPost} user={user} updateTopics={updateTopics} />
    </div>
  )
});

export default PostCreator;
