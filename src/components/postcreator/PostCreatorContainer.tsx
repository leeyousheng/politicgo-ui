import React, { useContext, useState } from 'react';
import { createPost } from '../../api/post';
import { Post } from '../../models/post';
import { UserContext } from '../provider';
import PostCreator from './PostCreator';

interface Props {
  insertPost: (post: Post) => void
}

const PostCreatorContainer = ({ insertPost }: Props) => {
  const [text, setText] = useState("");
  const [topics, setTopics] = useState<Array<string>>([]);
  const [modalOpen, setModalOpen] = useState(false);
  const user = useContext(UserContext);

  const updateText = (value: string) => {
    setText(value);
  };
  const openModal = () => {
    setModalOpen(true);
  };
  const closeModal = () => {
    setModalOpen(false);
  }
  const submitPost = (text: string) => {
    const callCreatePost = async () => {
      const post = await createPost(text, topics);
      if (!!post) {
        insertPost(post);
        updateText("");
        updateTopics([]);
        closeModal();
      }
    }
    callCreatePost();
  }
  const updateTopics = (topics: Array<string>) => {
    setTopics(topics);
  };

  if (user !== null) {
    return (
      <PostCreator
        text={text}
        modalOpen={modalOpen}
        updateText={updateText}
        openModal={openModal}
        closeModal={closeModal}
        submitPost={submitPost}
        topics={topics}
        updateTopics={updateTopics}
        user={user}
      />
    );
  }
  return null;
}

export default PostCreatorContainer;
