import { Button, Grid, Modal, Paper, withStyles } from '@material-ui/core';
import React, { ChangeEvent } from 'react';
import { creationModal as styles } from '../../../styles/feed';
import Header from './header';
import ProfileLine from './profileline';
import TextField from './textfield';
import Topicfield from './topicfield';

interface Props {
  classes: {
    root: string,
    form: string,
    header: string,
    headerText: string,
    closeIcon: string,
    profileLine: string,
    textfield: string,
    topicInput?: string
    topicDisplay?: string
  },
  open: boolean,
  text: string,
  user: firebase.User | null,
  topics: Array<string>,
  closeModal: () => void,
  updateText: (text: string) => void,
  submitPost: (text: string) => void,
  updateTopics: (topics: Array<string>) => void
};

const CreationModal = withStyles(styles)(({ classes, open, text, topics, user, closeModal, submitPost, updateText, updateTopics }: Props) => {
  const handleCloseButtonClick = () => closeModal();
  const handleOnModalClose = () => closeModal();
  const handleOnTextfieldChange = (evt: ChangeEvent<HTMLInputElement>) => updateText(evt.target.value);
  const handleOnSubmitPostButton = () => submitPost(text);

  return (
    <Modal open={open} className={classes.root} onClose={handleOnModalClose}>
      <Paper className={classes.form}>
        <Grid container spacing={2}>
          <Grid xs={12} item>
            <Header classes={classes} handleClose={handleCloseButtonClick} />
          </Grid>
          <Grid xs={12} item>
            <ProfileLine classes={classes} user={user} />
          </Grid>
          <Grid xs={12} item>
            <Topicfield classes={classes} topics={topics} updateTopics={updateTopics} />
          </Grid>
          <Grid xs={12} item>
            <TextField text={text} classes={{ root: classes.textfield }} onChange={handleOnTextfieldChange} />
          </Grid>
          <Grid xs={12} item>
            <Button fullWidth variant="contained" color="primary" onClick={handleOnSubmitPostButton}>Post</Button>
          </Grid>
        </Grid>
      </Paper>
    </Modal>
  )
});

export default CreationModal;
