import { IconButton, Typography } from '@material-ui/core';
import { Close as CloseIcon } from '@material-ui/icons';
import React from 'react';

interface Props {
  classes: {
    header: string,
    headerText: string,
    closeIcon: string,
  }
  handleClose: () => void,
};

const Header = ({ classes, handleClose }: Props) => (
  <div className={classes.header}>
    <Typography className={classes.headerText} variant="h2">What's New?</Typography>
    <IconButton className={classes.closeIcon} aria-label="close" onClick={handleClose}>
      <CloseIcon />
    </IconButton>
  </div>
);

export default Header;
