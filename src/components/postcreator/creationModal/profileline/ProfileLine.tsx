import { Avatar, Grid, Typography } from '@material-ui/core';
import React from 'react';

interface Props {
  classes: {
    profileLine: string;
  };
  user: firebase.User | null;
}

const ProfileLine = ({ classes, user }: Props) => (
  <div className={classes.profileLine} >
    <Grid container spacing={1}>
      <Grid item>
        <Avatar aria-label={user && user.displayName ? user.displayName : "anon"} src={user && user.photoURL ? user.photoURL : undefined} />
      </Grid>
      <Grid item>
        <Grid container>
          <Grid item xs={12}>
            <Typography>{user && user.displayName ? user.displayName : "anon"}</Typography>
          </Grid>
          <Grid item />
        </Grid>
      </Grid>
    </Grid>
  </div>
);

export default ProfileLine;
