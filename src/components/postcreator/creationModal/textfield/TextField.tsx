import { TextField as MuiTextField } from '@material-ui/core';
import React from 'react';

interface Props {
  classes: {
    root: string,
  },
  text: string,
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void,
};

const TextField = ({ classes, onChange, text }: Props) => (
  <div className={classes.root}>
    <MuiTextField
      autoFocus
      fullWidth
      label="Create your post"
      multiline
      rows={6}
      placeholder="Whats on your mind"
      variant="outlined"
      onChange={onChange}
      value={text}
    />
  </div>
)

export default TextField;
