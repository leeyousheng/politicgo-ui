import { Button, Chip, Grid, InputAdornment, TextField } from '@material-ui/core';
import { LocalOffer as LocalOfferIcon, NoteAdd as NoteAddIcon } from '@material-ui/icons';
import React from 'react';

interface Props {
  classes?: {
    topicInput?: string
    topicDisplay?: string
  }
  topics: Array<string>
  inputVal: string
  textChangeHandler: (event: React.ChangeEvent<HTMLInputElement>) => void,
  onSubmitHandler: () => void
  getDeleteHandler: (code: string) => () => void
}

const Topicfield = ({ classes, topics, inputVal, textChangeHandler, onSubmitHandler, getDeleteHandler }: Props) => (
  <div>
    <Grid container spacing={2}>
      <Grid xs item>
        <TextField
          className={classes?.topicInput}
          autoFocus
          fullWidth
          placeholder="Add some topic..."
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <LocalOfferIcon />
              </InputAdornment>
            )
          }}
          value={inputVal}
          onChange={textChangeHandler}
          onSubmit={onSubmitHandler}
        />
      </Grid>
      <Grid item>
        <Button
          variant="contained"
          size="small"
          color="secondary"
          endIcon={<NoteAddIcon />}
          onClick={onSubmitHandler}
        >
          Add Topic
        </Button>
      </Grid>
    </Grid>

    {topics.length > 0 ? (
      <Grid container className={classes?.topicDisplay}>
        {topics.map(t => (
          <Grid item>
            <Chip key={t} size="small" label={t} onDelete={getDeleteHandler(t)} />
          </Grid>))}
      </Grid>
    ) : undefined}
  </div>
);

export default Topicfield;
