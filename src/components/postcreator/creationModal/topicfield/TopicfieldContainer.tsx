import React, { useState } from 'react';
import Topicfield from './Topicfield';

interface Props {
  classes?: {
    topicInput?: string
    topicDisplay?: string
  }
  topics: Array<string>;
  updateTopics: (topics: Array<string>) => void
}

const TopicfieldContainer = ({ classes, topics, updateTopics }: Props) => {
  const [curTopic, setCurTopic] = useState("");

  const handleTopicfieldChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setCurTopic(event.target.value);
  }

  const handleOnSubmit = () => {
    if (curTopic !== '') {
      updateTopics(topics.concat(curTopic))
    }
    setCurTopic('');
  }

  const getDeleteHandler = (code: string) => () => {
    updateTopics(topics.filter(t => t !== code));
  }

  return (
    <Topicfield
      classes={classes}
      topics={topics}
      inputVal={curTopic}
      textChangeHandler={handleTopicfieldChange}
      onSubmitHandler={handleOnSubmit}
      getDeleteHandler={getDeleteHandler}
    />
  );
}

export default TopicfieldContainer;
