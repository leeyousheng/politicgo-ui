import { Button, withStyles } from '@material-ui/core';
import React from 'react';
import { Post as PostT } from '../../models/post';
import { posts as styles } from '../../styles/feed';
import Post from './post';

interface Props {
  posts: Array<PostT>,
  hasMore: boolean
  handleLoadMore: (cursor: string) => void
  classes: {
    posts: string,
  },
};

const Posts = withStyles(styles)(({ posts, classes, handleLoadMore, hasMore }: Props) => (
  <div className={classes.posts}>
    {posts.map(post => <Post key={post.id} post={post} />)}
    {hasMore ? <Button fullWidth color="primary" onClick={() => { handleLoadMore(posts[posts.length - 1].createdOn.toString()) }}>Load More</Button> : undefined}
  </div >
));

export default Posts;
