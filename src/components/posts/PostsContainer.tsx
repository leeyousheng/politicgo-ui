import React from 'react';
import { Post } from '../../models/post';
import Posts from './Posts';

interface Props {
  posts: Array<Post>
  hasMore: boolean
  retrievePosts: (cursor: string) => void
}

const PostsContainer = ({ posts, hasMore, retrievePosts }: Props) => {
  const handleLoadMore = (cursor: string) => retrievePosts(cursor);

  return <Posts posts={posts} hasMore={hasMore} handleLoadMore={handleLoadMore} />;
}

export default PostsContainer;
