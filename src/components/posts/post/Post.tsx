import { Card, withStyles } from '@material-ui/core';
import React from 'react';
import { post as styles } from '../../../styles/feed';
import PostAction from './PostAction';
import PostBar from './PostBar';
import PostContent from './PostContent';

interface Props {
  name: string,
  photoURL: string,
  createdOn: string,
  text: Array<string>,
  classes: {
    post: string,
    bar: string,
    textContent: string,
    action: string,
    paragraph: string,
  },
};

const Post = withStyles(styles)(({ name, photoURL, createdOn, text, classes }: Props) => (
  <Card className={classes.post} elevation={1}>
    <PostBar className={classes.bar} name={name} photoURL={photoURL} createdOn={createdOn} />
    <PostContent classes={classes} text={text} />
    <PostAction />
  </Card>
));

export default Post;
