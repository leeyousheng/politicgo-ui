import { Avatar, CardHeader, IconButton } from '@material-ui/core';
import { MoreVert as MoreVertIcon } from '@material-ui/icons';
import React from 'react';

interface Props {
  className?: string
  name: string
  createdOn: string
  photoURL?: string
}

const PostBar = ({ className, name, createdOn, photoURL }: Props) => (
  <CardHeader
    className={className}
    avatar={
      <Avatar src={photoURL} alt={name} />
    }
    action={
      <IconButton aria-label="settings">
        <MoreVertIcon />
      </IconButton>
    }
    title={name}
    subheader={createdOn}
  />
);

export default PostBar;
