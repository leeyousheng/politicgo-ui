import React from 'react';
import { Post as PostT } from '../../../models/post';
import { text, time } from '../../../util';
import Post from './Post';

interface Props {
  post: PostT,
};

const PostContainer = ({ post }: Props) => (
  <Post
    name={post.name}
    photoURL={post.photoURL}
    createdOn={time.format(post.createdOn)}
    text={text.toParagraphArray(post.text)}
  />
);

export default PostContainer;
