import { CardContent, Typography } from '@material-ui/core';
import React, { Fragment } from 'react';

interface Props {
  classes: {
    textContent: string,
    paragraph: string,
  },
  text: Array<string>,
};

const PostContent = ({ classes, text }: Props) => (
  <Fragment>
    <CardContent className={classes.textContent}>
      {text.map((para, i) => (
        <Typography key={i} className={classes.paragraph} variant="body2" component="p">
          {para}
        </Typography>
      ))}
    </CardContent>
  </Fragment>
);

export default PostContent;
