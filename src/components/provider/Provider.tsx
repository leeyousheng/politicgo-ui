import { User } from 'firebase';
import React from 'react';
import TokenSetProvider from './TokenSetProvider';
import UserProvider from './UserProvider';

interface Props {
  user: User | null
  isTokenSet: boolean
  children: React.ReactNode
}

const Provider = ({ user, isTokenSet, children }: Props) => (
  <UserProvider user={user}>
    <TokenSetProvider isTokenSet={isTokenSet}>
      {children}
    </TokenSetProvider>
  </UserProvider>
);

export default Provider;
