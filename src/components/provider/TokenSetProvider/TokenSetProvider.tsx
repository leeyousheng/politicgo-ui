import React from 'react';
import TokenSetContext from './context';

interface Props {
  isTokenSet: boolean,
  children: React.ReactNode,
};

const TokenSetProvider = ({ isTokenSet, children }: Props) => (
  <TokenSetContext.Provider value={isTokenSet}>
    {children}
  </TokenSetContext.Provider>
)

export default TokenSetProvider;
