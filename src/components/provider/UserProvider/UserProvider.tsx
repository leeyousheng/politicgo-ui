import React from 'react';
import UserContext from './context';

interface Props {
  user: firebase.User | null,
  children: React.ReactNode,
};

const UserProvider = ({ user, children }: Props) => (
  <UserContext.Provider value={user}>
    {children}
  </UserContext.Provider>
);

export default UserProvider;
