import { createContext } from 'react';

export default createContext<firebase.User | null>(null);
