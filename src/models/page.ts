export class Page<T> {
  items: Array<T>
  hasMore: boolean

  constructor(items: Array<T>, hasMore: boolean) {
    this.items = items;
    this.hasMore = hasMore;
  }
};
