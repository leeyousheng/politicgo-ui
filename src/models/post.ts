export class Post {
  name: string;
  photoURL: string;
  createdBy: string;
  createdOn: Date;
  id: string;
  text: string;
  topics: Array<string>

  constructor(name: string, photoURL: string, createdBy: string, createdOn: Date, id: string, text: string, topics: Array<string>) {
    this.name = name;
    this.photoURL = photoURL;
    this.createdBy = createdBy;
    this.createdOn = createdOn;
    this.id = id;
    this.text = text;
    this.topics = topics;
  }
}