import { render } from '@testing-library/react';
import React from 'react';
import Feed from './Feed';

test('renders a container with posts section', () => {
  const { getByTestId } = render(<Feed posts={[]} hasMore={false} insertPost={() => { }} retrievePosts={() => { }} />)
  expect(getByTestId('comp-posts')).toBeInTheDocument()
});