import { Grid, Hidden, withStyles } from '@material-ui/core';
import React from 'react';
import PostCreator from '../../components/postcreator';
import Posts from '../../components/posts';
import { Post } from '../../models/post';
import { feed as styles } from '../../styles/feed';

interface Props {
  classes: {
    feedSection: string,
  },
  posts: Array<Post>
  hasMore: boolean
  insertPost: (post: Post) => void
  retrievePosts: (cursor: string) => void
}

const Feed = withStyles(styles)(({ classes, posts, hasMore, insertPost, retrievePosts }: Props) => (
  <div data-testid="page-feed">
    <Grid container justify="center">
      <Hidden mdDown>
        <Grid item lg />
      </Hidden>
      <Grid item md style={{ maxWidth: "100%", width: "480px" }} lg>
        <div className={classes.feedSection}>
          <PostCreator insertPost={insertPost} />
          <Posts posts={posts} hasMore={hasMore} retrievePosts={retrievePosts} />
        </div>
      </Grid>
      <Hidden smDown>
        <Grid item md lg />
      </Hidden>
    </Grid>
  </div>
));

export default Feed;