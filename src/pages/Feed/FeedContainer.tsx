import React, { useContext, useEffect, useState } from 'react';
import { getFeed as apiGetFeed } from '../../api/post/feed';
import { TokenSetContext } from '../../components/provider';
import { Post } from '../../models/post';
import Feed from './Feed';

const FeedContainer = () => {
  const [posts, setPosts] = useState<Array<Post>>([]);
  const [hasMore, setHasMore] = useState<boolean>(false);
  const isTokenSet = useContext(TokenSetContext);

  const retrieveFeed = (cursor?: string) => {
    const getFeed = async () => {
      const page = await apiGetFeed(!!cursor ? posts[posts.length - 1].createdOn.toString() : undefined);
      if (!!page) {
        setPosts(posts.concat(page.items));
        setHasMore(page.hasMore);
      }
    }
    getFeed();
  }

  const useEffectHandler = () => {
    if (isTokenSet) {
      retrieveFeed();
    }
  }

  const insertPost = (post: Post) => setPosts([post].concat(posts));
  useEffect(useEffectHandler, [isTokenSet]);
  return (<Feed posts={posts} hasMore={hasMore} retrievePosts={retrieveFeed} insertPost={insertPost} />);
}

export default FeedContainer;
