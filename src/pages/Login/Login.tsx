import { Typography } from '@material-ui/core';
import React from 'react';
import LoginComponent from '../../components/login';

const Login = () => (
  <div>
    <Typography variant="h1"> Login</Typography>
    <Typography variant="body1">Please sign-in:</Typography>
    <LoginComponent />
  </div>
);

export default Login;

