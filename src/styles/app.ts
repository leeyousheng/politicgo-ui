import { createStyles } from "@material-ui/core";

const app = () => createStyles({
  app: {
    backgroundColor: '#f5f5f5',
  },
  content: {
    paddingTop: '64px',
  },
});

export { app };

