import { createStyles, fade, Theme } from "@material-ui/core/styles";

const appbar = () => createStyles({
  midSpace: {
    flexGrow: 1,
  },
});

const branding = ({ palette }: Theme) => createStyles({
  branding: {
    color: palette.primary.contrastText,
  },
});

const searchBar = (theme: Theme) => createStyles({
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    maxWidth: '260px',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 1),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
    width: '100%',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: '40px',
    transition: theme.transitions.create('width'),
    width: '100%',
    justifyItems: 'left'
  },
});

const profile = ({ spacing, palette }: Theme) => createStyles({
  button: {
    width: '36px',
    height: '36px',
    padding: '6px',
    marginLeft: spacing(1),
  },
  avatar: {
    width: '30px',
    height: '30px',
  },
  dropdown: {
    color: palette.primary.contrastText,
  },
});

const menu = () => createStyles({
  menu: {
    marginTop: '14px',
  },
  itemIcon: {
    minWidth: '30px',
  },
});

export {
  branding,
  searchBar,
  profile,
  menu,
  appbar
};

