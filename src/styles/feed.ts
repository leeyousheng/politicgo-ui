import { createStyles, Theme } from "@material-ui/core";

const feed = ({ spacing }: Theme) => createStyles({
  feedSection: {
    padding: spacing(2)
  }
})

const postCreator = ({ spacing }: Theme) => createStyles({
  root: {
    marginBottom: spacing(2),
    padding: '16px',
  },
  input: {
    height: "40px",
    width: "100%",
    caretColor: "transparent",
  },
});

const posts = () => createStyles({
  posts: {}
});

const post = ({ spacing }: Theme) => createStyles({
  post: {
    marginBottom: spacing(2),
  },
  bar: {
    textAlign: 'left',
  },
  textContent: {
    padding: '4px 16px 4px 16px',
  },
  paragraph: {
    marginBottom: spacing(1),
  },
  action: {}
});

const creationModal = ({ spacing }: Theme) => createStyles({
  root: {
    padding: "76px 8px 76px 8px",
    display: "flex",
    justifyContent: "center",
  },
  form: {
    width: "488px",
    minHeight: "500px",
    padding: "0px 16px 16px 16px",
    '&:focus': {
      outline: 'none',
    },
  },
  header: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: 60,
    position: 'relative',
    borderBottom: '1px solid grey',
  },
  headerText: {
    fontSize: "1.25rem",
    fontWeight: 700,
  },
  closeIcon: {
    position: "absolute",
    top: '12px',
    right: '16px',
    height: 36,
    width: 36,
  },
  profileLine: {},
  textfield: {},
  topicInput: {
    marginBottom: spacing(1)
  }
});

export {
  posts,
  post,
  feed,
  postCreator,
  creationModal,
};
