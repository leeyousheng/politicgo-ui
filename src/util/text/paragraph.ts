const toParagraphArray = (text: string, delimeter: string = '\n') => {
  return text.split(delimeter);
}

export { toParagraphArray };

