import moment from 'moment';

const format = (date: Date): string => {
  if (moment(date).isBefore(moment().subtract(1, "month"))) {
    return moment(date).format("MMM Do YYYY")
  }
  return moment(date).fromNow();
}

export {
  format
};
